﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace house
{
    public partial class Form1 : Form
    {

        List<Panel> panels = new List<Panel>();
        int index;

        private SerialPort rs232;
        private delegate void msgDelegate(byte[] b);
        private msgDelegate showReceivedMessage;


        private string portcom;
        private int BS;
        private int dataBits;
        private StopBits stopBits;
        private Parity parity;
        private int btnNo;
        private string btnId;
        private Boolean status;

      
        public Form1()
        {
            InitializeComponent();
            foreach (var button in panel_house.Controls.OfType<Button>())
            {
                button.BackColor = Color.Red;
              
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            panels.Add(panel_house);
            panels.Add(panel_Settings);
            panels[index].BringToFront();


            string[] ports = SerialPort.GetPortNames();
            cb_portCom.Items.AddRange(ports);

            cb_portCom.Text = "COM2";
            cb_baudRate.Text = "9600";
            cb_dataBits.Text = "8";
            cb_stopBits.Text = "One";
            cb_parity.Text = "None";
        }

        private void btn_OPEN_Click(object sender, EventArgs e)
        {
            

           
            //this.rs232.ReceivedBytesThreshold = 3;
            //this.rs232.DataReceived += Rs232_DataReceived;
 

            try
            {

                portcom = cb_portCom.Text;
                BS = Convert.ToInt32(cb_baudRate.Text);
                dataBits = Convert.ToInt32(cb_dataBits.Text);
                stopBits = (StopBits)Enum.Parse(typeof(StopBits), cb_stopBits.Text);
                parity = (Parity)Enum.Parse(typeof(Parity), cb_parity.Text);

                this.rs232 = new SerialPort(portcom, BS,
               parity, dataBits, stopBits);

                this.rs232.DataReceived += new SerialDataReceivedEventHandler(Rs232_DataReceived);
                if (!this.rs232.IsOpen)
                {
                    this.rs232.Open();
                    MessageBox.Show("opened");
                }
                

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);

            }
        }

        private void Rs232_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Byte[] msg = new Byte[3];

            for (int i = 0; i < 3; i++)
            {
                msg[i] = Convert.ToByte(this.rs232.ReadByte());
            }

            this.showReceivedMessage = new msgDelegate(ShowMsg);
            this.Invoke(showReceivedMessage, msg);
        }
        private void ShowMsg(byte[] b)
        {

            List<char> x = new List<char>();

            string str_byteReveived = System.Text.Encoding.UTF8.GetString(b); //byte to string

            foreach(var word in str_byteReveived)
            {
                x.Add(word);
            }

            int currentstatus_Reveived = char.Parse(x[2].ToString());

            string element_ID_received = x[0].ToString() + x[1].ToString();

           

           // var result = str_byteReveived.Length - 1;

           
            // Control Name_finder = this.Controls[str_byteReveived];

            Button Name_finder = this.panel_house.Controls.Find(element_ID_received, true).FirstOrDefault() as Button;

            

            if (currentstatus_Reveived == 1)
            {
                Name_finder.BackColor = Color.Green;
            }
            else if (currentstatus_Reveived == 0)
            {
                Name_finder.BackColor = Color.Red;
            }
            else
            {
                Name_finder.BackColor = Color.Green;
            }

            tb_RECEIVE.Text = b[0].ToString();
           // MessageBox.Show(b[1].ToString());
        }

        

        //OPEN SETTING BTN
        private void btn_OpenSetting_Click_1(object sender, EventArgs e)
        {
            if (index < panels.Count - 1)
            {
                panels[++index].BringToFront();
            }
        }

        private void btn_CloseSetting_Click(object sender, EventArgs e)
        {
            if (index > 0)
            {
                panels[--index].BringToFront();
            }
        }
    }
}
