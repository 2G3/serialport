﻿
namespace house
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_OPEN = new System.Windows.Forms.Button();
            this.btn_CLOSE = new System.Windows.Forms.Button();
            this.panel_house = new System.Windows.Forms.Panel();
            this.p1 = new System.Windows.Forms.Button();
            this.btn_OpenSetting = new System.Windows.Forms.Button();
            this.panel_Settings = new System.Windows.Forms.Panel();
            this.group_settings = new System.Windows.Forms.GroupBox();
            this.lb_parity = new System.Windows.Forms.Label();
            this.lb_stopBits = new System.Windows.Forms.Label();
            this.lb_dataBits = new System.Windows.Forms.Label();
            this.lb_baudRate = new System.Windows.Forms.Label();
            this.lb_portcom = new System.Windows.Forms.Label();
            this.cb_parity = new System.Windows.Forms.ComboBox();
            this.cb_stopBits = new System.Windows.Forms.ComboBox();
            this.cb_dataBits = new System.Windows.Forms.ComboBox();
            this.cb_baudRate = new System.Windows.Forms.ComboBox();
            this.cb_portCom = new System.Windows.Forms.ComboBox();
            this.btn_CloseSetting = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.p2 = new System.Windows.Forms.Button();
            this.p3 = new System.Windows.Forms.Button();
            this.p4 = new System.Windows.Forms.Button();
            this.f1 = new System.Windows.Forms.Button();
            this.f2 = new System.Windows.Forms.Button();
            this.f3 = new System.Windows.Forms.Button();
            this.f4 = new System.Windows.Forms.Button();
            this.f5 = new System.Windows.Forms.Button();
            this.panel_house.SuspendLayout();
            this.panel_Settings.SuspendLayout();
            this.group_settings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_OPEN
            // 
            this.btn_OPEN.Location = new System.Drawing.Point(1155, 130);
            this.btn_OPEN.Name = "btn_OPEN";
            this.btn_OPEN.Size = new System.Drawing.Size(75, 48);
            this.btn_OPEN.TabIndex = 0;
            this.btn_OPEN.Text = "OPEN";
            this.btn_OPEN.UseVisualStyleBackColor = true;
            this.btn_OPEN.Click += new System.EventHandler(this.btn_OPEN_Click);
            // 
            // btn_CLOSE
            // 
            this.btn_CLOSE.Location = new System.Drawing.Point(1155, 184);
            this.btn_CLOSE.Name = "btn_CLOSE";
            this.btn_CLOSE.Size = new System.Drawing.Size(75, 45);
            this.btn_CLOSE.TabIndex = 1;
            this.btn_CLOSE.Text = "CLOSE";
            this.btn_CLOSE.UseVisualStyleBackColor = true;
            // 
            // panel_house
            // 
            this.panel_house.Controls.Add(this.f5);
            this.panel_house.Controls.Add(this.f4);
            this.panel_house.Controls.Add(this.f3);
            this.panel_house.Controls.Add(this.f2);
            this.panel_house.Controls.Add(this.f1);
            this.panel_house.Controls.Add(this.p4);
            this.panel_house.Controls.Add(this.p3);
            this.panel_house.Controls.Add(this.p2);
            this.panel_house.Controls.Add(this.p1);
            this.panel_house.Controls.Add(this.pictureBox1);
            this.panel_house.Location = new System.Drawing.Point(11, 61);
            this.panel_house.Name = "panel_house";
            this.panel_house.Size = new System.Drawing.Size(1094, 611);
            this.panel_house.TabIndex = 6;
            // 
            // p1
            // 
            this.p1.Location = new System.Drawing.Point(361, 77);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(104, 33);
            this.p1.TabIndex = 0;
            this.p1.Text = "P1";
            this.p1.UseVisualStyleBackColor = true;
            // 
            // btn_OpenSetting
            // 
            this.btn_OpenSetting.Location = new System.Drawing.Point(11, -6);
            this.btn_OpenSetting.Name = "btn_OpenSetting";
            this.btn_OpenSetting.Size = new System.Drawing.Size(75, 39);
            this.btn_OpenSetting.TabIndex = 7;
            this.btn_OpenSetting.Text = "Setting";
            this.btn_OpenSetting.UseVisualStyleBackColor = true;
            this.btn_OpenSetting.Click += new System.EventHandler(this.btn_OpenSetting_Click_1);
            // 
            // panel_Settings
            // 
            this.panel_Settings.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel_Settings.Controls.Add(this.group_settings);
            this.panel_Settings.Controls.Add(this.btn_CloseSetting);
            this.panel_Settings.Location = new System.Drawing.Point(754, 61);
            this.panel_Settings.Name = "panel_Settings";
            this.panel_Settings.Size = new System.Drawing.Size(348, 587);
            this.panel_Settings.TabIndex = 2;
            // 
            // group_settings
            // 
            this.group_settings.Controls.Add(this.lb_parity);
            this.group_settings.Controls.Add(this.lb_stopBits);
            this.group_settings.Controls.Add(this.lb_dataBits);
            this.group_settings.Controls.Add(this.lb_baudRate);
            this.group_settings.Controls.Add(this.lb_portcom);
            this.group_settings.Controls.Add(this.cb_parity);
            this.group_settings.Controls.Add(this.cb_stopBits);
            this.group_settings.Controls.Add(this.cb_dataBits);
            this.group_settings.Controls.Add(this.cb_baudRate);
            this.group_settings.Controls.Add(this.cb_portCom);
            this.group_settings.Location = new System.Drawing.Point(10, 85);
            this.group_settings.Name = "group_settings";
            this.group_settings.Size = new System.Drawing.Size(316, 451);
            this.group_settings.TabIndex = 1;
            this.group_settings.TabStop = false;
            this.group_settings.Text = "Settings";
            // 
            // lb_parity
            // 
            this.lb_parity.AutoSize = true;
            this.lb_parity.Location = new System.Drawing.Point(6, 280);
            this.lb_parity.Name = "lb_parity";
            this.lb_parity.Size = new System.Drawing.Size(67, 20);
            this.lb_parity.TabIndex = 9;
            this.lb_parity.Text = "PARITY";
            // 
            // lb_stopBits
            // 
            this.lb_stopBits.AutoSize = true;
            this.lb_stopBits.Location = new System.Drawing.Point(6, 213);
            this.lb_stopBits.Name = "lb_stopBits";
            this.lb_stopBits.Size = new System.Drawing.Size(91, 20);
            this.lb_stopBits.TabIndex = 8;
            this.lb_stopBits.Text = "STOP BITS";
            // 
            // lb_dataBits
            // 
            this.lb_dataBits.AutoSize = true;
            this.lb_dataBits.Location = new System.Drawing.Point(6, 156);
            this.lb_dataBits.Name = "lb_dataBits";
            this.lb_dataBits.Size = new System.Drawing.Size(92, 20);
            this.lb_dataBits.TabIndex = 7;
            this.lb_dataBits.Text = "DATA BITS";
            // 
            // lb_baudRate
            // 
            this.lb_baudRate.AutoSize = true;
            this.lb_baudRate.Location = new System.Drawing.Point(6, 103);
            this.lb_baudRate.Name = "lb_baudRate";
            this.lb_baudRate.Size = new System.Drawing.Size(24, 20);
            this.lb_baudRate.TabIndex = 6;
            this.lb_baudRate.Text = "/S";
            // 
            // lb_portcom
            // 
            this.lb_portcom.AutoSize = true;
            this.lb_portcom.Location = new System.Drawing.Point(6, 52);
            this.lb_portcom.Name = "lb_portcom";
            this.lb_portcom.Size = new System.Drawing.Size(92, 20);
            this.lb_portcom.TabIndex = 5;
            this.lb_portcom.Text = "PORT COM";
            // 
            // cb_parity
            // 
            this.cb_parity.FormattingEnabled = true;
            this.cb_parity.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even"});
            this.cb_parity.Location = new System.Drawing.Point(119, 272);
            this.cb_parity.Name = "cb_parity";
            this.cb_parity.Size = new System.Drawing.Size(175, 28);
            this.cb_parity.TabIndex = 4;
            // 
            // cb_stopBits
            // 
            this.cb_stopBits.FormattingEnabled = true;
            this.cb_stopBits.Items.AddRange(new object[] {
            "One",
            "Two"});
            this.cb_stopBits.Location = new System.Drawing.Point(119, 210);
            this.cb_stopBits.Name = "cb_stopBits";
            this.cb_stopBits.Size = new System.Drawing.Size(175, 28);
            this.cb_stopBits.TabIndex = 3;
            // 
            // cb_dataBits
            // 
            this.cb_dataBits.FormattingEnabled = true;
            this.cb_dataBits.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.cb_dataBits.Location = new System.Drawing.Point(119, 148);
            this.cb_dataBits.Name = "cb_dataBits";
            this.cb_dataBits.Size = new System.Drawing.Size(175, 28);
            this.cb_dataBits.TabIndex = 2;
            // 
            // cb_baudRate
            // 
            this.cb_baudRate.FormattingEnabled = true;
            this.cb_baudRate.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600"});
            this.cb_baudRate.Location = new System.Drawing.Point(118, 95);
            this.cb_baudRate.Name = "cb_baudRate";
            this.cb_baudRate.Size = new System.Drawing.Size(175, 28);
            this.cb_baudRate.TabIndex = 1;
            // 
            // cb_portCom
            // 
            this.cb_portCom.FormattingEnabled = true;
            this.cb_portCom.Location = new System.Drawing.Point(119, 44);
            this.cb_portCom.Name = "cb_portCom";
            this.cb_portCom.Size = new System.Drawing.Size(180, 28);
            this.cb_portCom.TabIndex = 0;
            // 
            // btn_CloseSetting
            // 
            this.btn_CloseSetting.BackColor = System.Drawing.Color.Maroon;
            this.btn_CloseSetting.Location = new System.Drawing.Point(0, 0);
            this.btn_CloseSetting.Name = "btn_CloseSetting";
            this.btn_CloseSetting.Size = new System.Drawing.Size(97, 55);
            this.btn_CloseSetting.TabIndex = 0;
            this.btn_CloseSetting.Text = "Close";
            this.btn_CloseSetting.UseVisualStyleBackColor = false;
            this.btn_CloseSetting.Click += new System.EventHandler(this.btn_CloseSetting_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::house.Properties.Resources.m2;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1076, 601);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // p2
            // 
            this.p2.Location = new System.Drawing.Point(298, 129);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(33, 63);
            this.p2.TabIndex = 2;
            this.p2.Text = "P2";
            this.p2.UseVisualStyleBackColor = true;
            // 
            // p3
            // 
            this.p3.Location = new System.Drawing.Point(541, 260);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(33, 63);
            this.p3.TabIndex = 3;
            this.p3.Text = "P3";
            this.p3.UseVisualStyleBackColor = true;
            // 
            // p4
            // 
            this.p4.Location = new System.Drawing.Point(813, 388);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(38, 53);
            this.p4.TabIndex = 4;
            this.p4.Text = "P4";
            this.p4.UseVisualStyleBackColor = true;
            // 
            // f1
            // 
            this.f1.Location = new System.Drawing.Point(949, 123);
            this.f1.Name = "f1";
            this.f1.Size = new System.Drawing.Size(33, 85);
            this.f1.TabIndex = 5;
            this.f1.Text = "F1";
            this.f1.UseVisualStyleBackColor = true;
            // 
            // f2
            // 
            this.f2.Location = new System.Drawing.Point(949, 357);
            this.f2.Name = "f2";
            this.f2.Size = new System.Drawing.Size(33, 63);
            this.f2.TabIndex = 6;
            this.f2.Text = "F2";
            this.f2.UseVisualStyleBackColor = true;
            // 
            // f3
            // 
            this.f3.Location = new System.Drawing.Point(731, 440);
            this.f3.Name = "f3";
            this.f3.Size = new System.Drawing.Size(66, 33);
            this.f3.TabIndex = 7;
            this.f3.Text = "F3";
            this.f3.UseVisualStyleBackColor = true;
            // 
            // f4
            // 
            this.f4.Location = new System.Drawing.Point(444, 453);
            this.f4.Name = "f4";
            this.f4.Size = new System.Drawing.Size(66, 33);
            this.f4.TabIndex = 8;
            this.f4.Text = "F4";
            this.f4.UseVisualStyleBackColor = true;
            // 
            // f5
            // 
            this.f5.Location = new System.Drawing.Point(197, 453);
            this.f5.Name = "f5";
            this.f5.Size = new System.Drawing.Size(66, 33);
            this.f5.TabIndex = 9;
            this.f5.Text = "F5";
            this.f5.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1242, 677);
            this.Controls.Add(this.panel_Settings);
            this.Controls.Add(this.btn_OpenSetting);
            this.Controls.Add(this.btn_CLOSE);
            this.Controls.Add(this.btn_OPEN);
            this.Controls.Add(this.panel_house);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_house.ResumeLayout(false);
            this.panel_Settings.ResumeLayout(false);
            this.group_settings.ResumeLayout(false);
            this.group_settings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_OPEN;
        private System.Windows.Forms.Button btn_CLOSE;
        private System.Windows.Forms.Panel panel_house;
        private System.Windows.Forms.Panel panel_Settings;
        private System.Windows.Forms.GroupBox group_settings;
        private System.Windows.Forms.Label lb_parity;
        private System.Windows.Forms.Label lb_stopBits;
        private System.Windows.Forms.Label lb_dataBits;
        private System.Windows.Forms.Label lb_baudRate;
        private System.Windows.Forms.Label lb_portcom;
        private System.Windows.Forms.ComboBox cb_parity;
        private System.Windows.Forms.ComboBox cb_stopBits;
        private System.Windows.Forms.ComboBox cb_dataBits;
        private System.Windows.Forms.ComboBox cb_baudRate;
        private System.Windows.Forms.ComboBox cb_portCom;
        private System.Windows.Forms.Button btn_CloseSetting;
        private System.Windows.Forms.Button btn_OpenSetting;
        private System.Windows.Forms.Button p1;
        private System.Windows.Forms.Button p4;
        private System.Windows.Forms.Button p3;
        private System.Windows.Forms.Button p2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button f5;
        private System.Windows.Forms.Button f4;
        private System.Windows.Forms.Button f3;
        private System.Windows.Forms.Button f2;
        private System.Windows.Forms.Button f1;
    }
}

