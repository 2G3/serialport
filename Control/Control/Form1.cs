﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;

namespace Control
{
    public partial class Form1 : Form
    {
        List<Panel> listPanel = new List<Panel>();
        int index;

        private SerialPort rs232;

        private string portcom;
        private int BS;
        private int dataBits;
        private string stopBits;
        private Parity parity;
        private int btnNo;
        private string btnId;
        private Boolean status;
        public Form1()
        {
            InitializeComponent();

            foreach (var button in Controls.OfType<Button>())
            {
                button.BackColor = Color.Red;
                button.Click += button_Click;
            }


        }
        private void Form1_Load(object sender, EventArgs e)
        {
            listPanel.Add(panel1);
            listPanel.Add(panel2);
            listPanel[index].BringToFront();
        }
        private void button_Click(object sender, EventArgs eventArgs)
        {

            btnId = ((Button)sender).Name; //get click btn by str

            Button ClickedBtn = this.Controls.Find(btnId, true).FirstOrDefault() as Button; //get associate btn clicked


             char[] charArray = btnId.ToCharArray(); //tansform letter to char and plit word


           // string str = new string(charArray); //char to str

            btnNo = 001;
            SendData(btnNo, ClickedBtn, charArray);



        }

        //SEND DATA
        public void SendData(int data, Button ClickedBtn, char[] charArray)
        {


            Byte[] msgToSend = new Byte[3];
            try
            {


                // this.rs232.Write(msgToSend, 0, 2);

                msgToSend[0] = Convert.ToByte(data);
                msgToSend[1] = Convert.ToByte(charArray[0]);
                msgToSend[2] = Convert.ToByte(charArray[1]);

                if (ClickedBtn.BackColor == Color.Red)
                {
                    ClickedBtn.BackColor = Color.Green;
                }
                else if (ClickedBtn.BackColor == Color.Green)
                {
                    ClickedBtn.BackColor = Color.Red;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void f1_Click(object sender, EventArgs e)
        {

        }

        

        
    }
}
