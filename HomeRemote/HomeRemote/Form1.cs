﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;


namespace HomeRemote
{
    public partial class HousseRemote : Form
    {
        List<Panel> panels = new List<Panel>();
        int index;


        private SerialPort rs232;

        private string portcom;
        private int BS;
        private int dataBits;
        private StopBits stopBits;
        private Parity parity;
        private int btnNo;
        private string btnId;
        private Boolean status;
        private int currentStatus;

        public HousseRemote()
        {
            InitializeComponent();
            foreach (var button in panel_Remote.Controls.OfType<Button>())
            {
                button.BackColor = Color.Red;
                button.Click += button_Click;
            }
        }

        //FORM LOAD
        private void Form1_Load(object sender, EventArgs e)
        {
            panels.Add(panel_Remote);
            panels.Add(panel_Settings);
            panels[index].BringToFront();


            string[] ports = SerialPort.GetPortNames();
            cb_portCom.Items.AddRange(ports);

            cb_portCom.Text = "COM1";
            cb_baudRate.Text = "9600";
            cb_dataBits.Text = "8";
            cb_stopBits.Text = "One";
            cb_parity.Text = "None";
          

        }

        

        private void button_Click(object sender, EventArgs eventArgs)
        {

            btnId = ((Button)sender).Name; //get click btn by str

            Control ctn = this.panel_Remote.Controls[btnId]; //compare this btn if exist on form (little like a get element by id)

             char[] charArray = btnId.ToCharArray(); //tanfors letter to char and plit world


            // int val = charArray[1]; 

            SendData(charArray, ctn);

            //if (status == true)
            //{
            //    btnNo = 001;
            //    SendData(btnNo, ctn);

            //}



            switch (((Button)sender).Name)
            {
                case "f1":
                    //
                    break;
            }
        }

        //SEND DATA FUNCTION
        public void SendData(char[] arrayChar, Control ctn)
        {


            Byte[] msgToSend = new Byte[3];
            try
            {
                


                if (ctn.BackColor == Color.Red)
                {
                    currentStatus = 1;
                    ctn.BackColor = Color.Green;
                }
                else if (ctn.BackColor == Color.Green)
                {
                    currentStatus = 0;
                    ctn.BackColor = Color.Red;
                }
                msgToSend[0] = Convert.ToByte(arrayChar[0]);
                msgToSend[1] = Convert.ToByte(arrayChar[1]);
                msgToSend[2] = Convert.ToByte(currentStatus);

                this.rs232.Write(msgToSend, 0, 3);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //OPEN SETTING BTN
        private void btn_OpenSetting_Click(object sender, EventArgs e)
        {
            if(index < panels.Count - 1)
            {
                panels[++index].BringToFront();
            }
        }    

        //CLOSE SETTINGS BTN
        private void btn_CloseSetting_Click_1(object sender, EventArgs e)
        {
            if (index > 0)
            {
                panels[--index].BringToFront();
            }
        }

        private void panel_Remote_Paint(object sender, PaintEventArgs e)
        {
          
        }

        //OPEN PORT BTN
        private void btn_Open_Click(object sender, EventArgs e)
        {
            try
            {
                portcom = cb_portCom.Text;
                BS = Convert.ToInt32(cb_baudRate.Text);
                dataBits = Convert.ToInt32(cb_dataBits.Text);
                stopBits = (StopBits)Enum.Parse(typeof(StopBits), cb_stopBits.Text);
                parity = (Parity)Enum.Parse(typeof(Parity), cb_parity.Text);


                this.rs232 = new SerialPort(portcom, BS, parity, dataBits, stopBits);
                if(!this.rs232.IsOpen)
                {
                    this.rs232.Open();
                    MessageBox.Show("opened");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //CLOSE PORT BTN
        private void btn_close_Click(object sender, EventArgs e)
        {
            if (this.rs232.IsOpen)
            {
                this.rs232.Close();
                MessageBox.Show("closed");

            }
        }
    }
}
