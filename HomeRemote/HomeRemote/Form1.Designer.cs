﻿
namespace HomeRemote
{
    partial class HousseRemote
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Remote = new System.Windows.Forms.Panel();
            this.f3 = new System.Windows.Forms.Button();
            this.f2 = new System.Windows.Forms.Button();
            this.p3 = new System.Windows.Forms.Button();
            this.p1 = new System.Windows.Forms.Button();
            this.p4 = new System.Windows.Forms.Button();
            this.f4 = new System.Windows.Forms.Button();
            this.f5 = new System.Windows.Forms.Button();
            this.p2 = new System.Windows.Forms.Button();
            this.f1 = new System.Windows.Forms.Button();
            this.HousePlan = new System.Windows.Forms.PictureBox();
            this.btn_CloseSetting = new System.Windows.Forms.Button();
            this.btn_OpenSetting = new System.Windows.Forms.Button();
            this.panel_Settings = new System.Windows.Forms.Panel();
            this.group_settings = new System.Windows.Forms.GroupBox();
            this.lb_parity = new System.Windows.Forms.Label();
            this.lb_stopBits = new System.Windows.Forms.Label();
            this.lb_dataBits = new System.Windows.Forms.Label();
            this.lb_baudRate = new System.Windows.Forms.Label();
            this.lb_portcom = new System.Windows.Forms.Label();
            this.cb_parity = new System.Windows.Forms.ComboBox();
            this.cb_stopBits = new System.Windows.Forms.ComboBox();
            this.cb_dataBits = new System.Windows.Forms.ComboBox();
            this.cb_baudRate = new System.Windows.Forms.ComboBox();
            this.cb_portCom = new System.Windows.Forms.ComboBox();
            this.btn_Open = new System.Windows.Forms.Button();
            this.btn_close = new System.Windows.Forms.Button();
            this.panel_Remote.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HousePlan)).BeginInit();
            this.panel_Settings.SuspendLayout();
            this.group_settings.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Remote
            // 
            this.panel_Remote.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.panel_Remote.Controls.Add(this.f3);
            this.panel_Remote.Controls.Add(this.f2);
            this.panel_Remote.Controls.Add(this.p3);
            this.panel_Remote.Controls.Add(this.p1);
            this.panel_Remote.Controls.Add(this.p4);
            this.panel_Remote.Controls.Add(this.f4);
            this.panel_Remote.Controls.Add(this.f5);
            this.panel_Remote.Controls.Add(this.p2);
            this.panel_Remote.Controls.Add(this.f1);
            this.panel_Remote.Controls.Add(this.HousePlan);
            this.panel_Remote.Location = new System.Drawing.Point(12, 46);
            this.panel_Remote.Name = "panel_Remote";
            this.panel_Remote.Size = new System.Drawing.Size(1145, 673);
            this.panel_Remote.TabIndex = 0;
            this.panel_Remote.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Remote_Paint);
            // 
            // f3
            // 
            this.f3.Location = new System.Drawing.Point(539, 508);
            this.f3.Name = "f3";
            this.f3.Size = new System.Drawing.Size(62, 28);
            this.f3.TabIndex = 11;
            this.f3.Text = "F3";
            this.f3.UseVisualStyleBackColor = true;
            // 
            // f2
            // 
            this.f2.Location = new System.Drawing.Point(699, 403);
            this.f2.Name = "f2";
            this.f2.Size = new System.Drawing.Size(38, 50);
            this.f2.TabIndex = 10;
            this.f2.Text = "F2";
            this.f2.UseVisualStyleBackColor = true;
            // 
            // p3
            // 
            this.p3.Location = new System.Drawing.Point(397, 295);
            this.p3.Name = "p3";
            this.p3.Size = new System.Drawing.Size(34, 50);
            this.p3.TabIndex = 9;
            this.p3.Text = "P3";
            this.p3.UseVisualStyleBackColor = true;
            // 
            // p1
            // 
            this.p1.Location = new System.Drawing.Point(264, 85);
            this.p1.Name = "p1";
            this.p1.Size = new System.Drawing.Size(75, 33);
            this.p1.TabIndex = 1;
            this.p1.Text = "P1";
            this.p1.UseVisualStyleBackColor = true;
            // 
            // p4
            // 
            this.p4.Location = new System.Drawing.Point(603, 427);
            this.p4.Name = "p4";
            this.p4.Size = new System.Drawing.Size(35, 50);
            this.p4.TabIndex = 8;
            this.p4.Text = "P4";
            this.p4.UseVisualStyleBackColor = true;
            // 
            // f4
            // 
            this.f4.Location = new System.Drawing.Point(325, 508);
            this.f4.Name = "f4";
            this.f4.Size = new System.Drawing.Size(67, 28);
            this.f4.TabIndex = 5;
            this.f4.Text = "F4";
            this.f4.UseVisualStyleBackColor = true;
            // 
            // f5
            // 
            this.f5.Location = new System.Drawing.Point(141, 508);
            this.f5.Name = "f5";
            this.f5.Size = new System.Drawing.Size(69, 28);
            this.f5.TabIndex = 4;
            this.f5.Text = "F5";
            this.f5.UseVisualStyleBackColor = true;
            // 
            // p2
            // 
            this.p2.Location = new System.Drawing.Point(220, 146);
            this.p2.Name = "p2";
            this.p2.Size = new System.Drawing.Size(35, 65);
            this.p2.TabIndex = 3;
            this.p2.Text = "P2";
            this.p2.UseVisualStyleBackColor = true;
            // 
            // f1
            // 
            this.f1.Location = new System.Drawing.Point(699, 146);
            this.f1.Name = "f1";
            this.f1.Size = new System.Drawing.Size(38, 85);
            this.f1.TabIndex = 2;
            this.f1.Text = "F1";
            this.f1.UseVisualStyleBackColor = true;
            // 
            // HousePlan
            // 
            this.HousePlan.Image = global::HomeRemote.Properties.Resources.m2;
            this.HousePlan.Location = new System.Drawing.Point(3, 3);
            this.HousePlan.Name = "HousePlan";
            this.HousePlan.Size = new System.Drawing.Size(798, 667);
            this.HousePlan.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.HousePlan.TabIndex = 0;
            this.HousePlan.TabStop = false;
            // 
            // btn_CloseSetting
            // 
            this.btn_CloseSetting.BackColor = System.Drawing.Color.Maroon;
            this.btn_CloseSetting.Location = new System.Drawing.Point(0, 0);
            this.btn_CloseSetting.Name = "btn_CloseSetting";
            this.btn_CloseSetting.Size = new System.Drawing.Size(97, 55);
            this.btn_CloseSetting.TabIndex = 0;
            this.btn_CloseSetting.Text = "Close";
            this.btn_CloseSetting.UseVisualStyleBackColor = false;
            this.btn_CloseSetting.Click += new System.EventHandler(this.btn_CloseSetting_Click_1);
            // 
            // btn_OpenSetting
            // 
            this.btn_OpenSetting.Location = new System.Drawing.Point(12, -2);
            this.btn_OpenSetting.Name = "btn_OpenSetting";
            this.btn_OpenSetting.Size = new System.Drawing.Size(75, 39);
            this.btn_OpenSetting.TabIndex = 3;
            this.btn_OpenSetting.Text = "Setting";
            this.btn_OpenSetting.UseVisualStyleBackColor = true;
            this.btn_OpenSetting.Click += new System.EventHandler(this.btn_OpenSetting_Click);
            // 
            // panel_Settings
            // 
            this.panel_Settings.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.panel_Settings.Controls.Add(this.group_settings);
            this.panel_Settings.Controls.Add(this.btn_CloseSetting);
            this.panel_Settings.Location = new System.Drawing.Point(809, 46);
            this.panel_Settings.Name = "panel_Settings";
            this.panel_Settings.Size = new System.Drawing.Size(348, 670);
            this.panel_Settings.TabIndex = 1;
            // 
            // group_settings
            // 
            this.group_settings.Controls.Add(this.lb_parity);
            this.group_settings.Controls.Add(this.lb_stopBits);
            this.group_settings.Controls.Add(this.lb_dataBits);
            this.group_settings.Controls.Add(this.lb_baudRate);
            this.group_settings.Controls.Add(this.lb_portcom);
            this.group_settings.Controls.Add(this.cb_parity);
            this.group_settings.Controls.Add(this.cb_stopBits);
            this.group_settings.Controls.Add(this.cb_dataBits);
            this.group_settings.Controls.Add(this.cb_baudRate);
            this.group_settings.Controls.Add(this.cb_portCom);
            this.group_settings.Location = new System.Drawing.Point(10, 85);
            this.group_settings.Name = "group_settings";
            this.group_settings.Size = new System.Drawing.Size(316, 451);
            this.group_settings.TabIndex = 1;
            this.group_settings.TabStop = false;
            this.group_settings.Text = "Settings";
            // 
            // lb_parity
            // 
            this.lb_parity.AutoSize = true;
            this.lb_parity.Location = new System.Drawing.Point(6, 280);
            this.lb_parity.Name = "lb_parity";
            this.lb_parity.Size = new System.Drawing.Size(67, 20);
            this.lb_parity.TabIndex = 9;
            this.lb_parity.Text = "PARITY";
            // 
            // lb_stopBits
            // 
            this.lb_stopBits.AutoSize = true;
            this.lb_stopBits.Location = new System.Drawing.Point(6, 213);
            this.lb_stopBits.Name = "lb_stopBits";
            this.lb_stopBits.Size = new System.Drawing.Size(91, 20);
            this.lb_stopBits.TabIndex = 8;
            this.lb_stopBits.Text = "STOP BITS";
            // 
            // lb_dataBits
            // 
            this.lb_dataBits.AutoSize = true;
            this.lb_dataBits.Location = new System.Drawing.Point(6, 156);
            this.lb_dataBits.Name = "lb_dataBits";
            this.lb_dataBits.Size = new System.Drawing.Size(92, 20);
            this.lb_dataBits.TabIndex = 7;
            this.lb_dataBits.Text = "DATA BITS";
            // 
            // lb_baudRate
            // 
            this.lb_baudRate.AutoSize = true;
            this.lb_baudRate.Location = new System.Drawing.Point(6, 103);
            this.lb_baudRate.Name = "lb_baudRate";
            this.lb_baudRate.Size = new System.Drawing.Size(24, 20);
            this.lb_baudRate.TabIndex = 6;
            this.lb_baudRate.Text = "/S";
            // 
            // lb_portcom
            // 
            this.lb_portcom.AutoSize = true;
            this.lb_portcom.Location = new System.Drawing.Point(6, 52);
            this.lb_portcom.Name = "lb_portcom";
            this.lb_portcom.Size = new System.Drawing.Size(92, 20);
            this.lb_portcom.TabIndex = 5;
            this.lb_portcom.Text = "PORT COM";
            // 
            // cb_parity
            // 
            this.cb_parity.FormattingEnabled = true;
            this.cb_parity.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even"});
            this.cb_parity.Location = new System.Drawing.Point(119, 272);
            this.cb_parity.Name = "cb_parity";
            this.cb_parity.Size = new System.Drawing.Size(175, 28);
            this.cb_parity.TabIndex = 4;
            // 
            // cb_stopBits
            // 
            this.cb_stopBits.FormattingEnabled = true;
            this.cb_stopBits.Items.AddRange(new object[] {
            "One",
            "Two"});
            this.cb_stopBits.Location = new System.Drawing.Point(119, 210);
            this.cb_stopBits.Name = "cb_stopBits";
            this.cb_stopBits.Size = new System.Drawing.Size(175, 28);
            this.cb_stopBits.TabIndex = 3;
            // 
            // cb_dataBits
            // 
            this.cb_dataBits.FormattingEnabled = true;
            this.cb_dataBits.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.cb_dataBits.Location = new System.Drawing.Point(119, 148);
            this.cb_dataBits.Name = "cb_dataBits";
            this.cb_dataBits.Size = new System.Drawing.Size(175, 28);
            this.cb_dataBits.TabIndex = 2;
            // 
            // cb_baudRate
            // 
            this.cb_baudRate.FormattingEnabled = true;
            this.cb_baudRate.Items.AddRange(new object[] {
            "2400",
            "4800",
            "9600"});
            this.cb_baudRate.Location = new System.Drawing.Point(118, 95);
            this.cb_baudRate.Name = "cb_baudRate";
            this.cb_baudRate.Size = new System.Drawing.Size(175, 28);
            this.cb_baudRate.TabIndex = 1;
            // 
            // cb_portCom
            // 
            this.cb_portCom.FormattingEnabled = true;
            this.cb_portCom.Location = new System.Drawing.Point(119, 44);
            this.cb_portCom.Name = "cb_portCom";
            this.cb_portCom.Size = new System.Drawing.Size(180, 28);
            this.cb_portCom.TabIndex = 0;
            // 
            // btn_Open
            // 
            this.btn_Open.BackColor = System.Drawing.Color.CadetBlue;
            this.btn_Open.Location = new System.Drawing.Point(137, -2);
            this.btn_Open.Name = "btn_Open";
            this.btn_Open.Size = new System.Drawing.Size(70, 39);
            this.btn_Open.TabIndex = 4;
            this.btn_Open.Text = "OPEN";
            this.btn_Open.UseMnemonic = false;
            this.btn_Open.UseVisualStyleBackColor = false;
            this.btn_Open.Click += new System.EventHandler(this.btn_Open_Click);
            // 
            // btn_close
            // 
            this.btn_close.BackColor = System.Drawing.Color.IndianRed;
            this.btn_close.Location = new System.Drawing.Point(213, -2);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(71, 39);
            this.btn_close.TabIndex = 5;
            this.btn_close.Text = "CLOSE";
            this.btn_close.UseVisualStyleBackColor = false;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // HousseRemote
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1248, 731);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.btn_Open);
            this.Controls.Add(this.panel_Settings);
            this.Controls.Add(this.panel_Remote);
            this.Controls.Add(this.btn_OpenSetting);
            this.Name = "HousseRemote";
            this.Text = "Housse Remote";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Remote.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.HousePlan)).EndInit();
            this.panel_Settings.ResumeLayout(false);
            this.group_settings.ResumeLayout(false);
            this.group_settings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Remote;
        private System.Windows.Forms.Button btn_CloseSetting;
        private System.Windows.Forms.Button btn_OpenSetting;
        private System.Windows.Forms.Panel panel_Settings;
        private System.Windows.Forms.PictureBox HousePlan;
        private System.Windows.Forms.Button p3;
        private System.Windows.Forms.Button p4;
        private System.Windows.Forms.Button f4;
        private System.Windows.Forms.Button f5;
        private System.Windows.Forms.Button p2;
        private System.Windows.Forms.Button f1;
        private System.Windows.Forms.Button p1;
        private System.Windows.Forms.Button f2;
        private System.Windows.Forms.Button f3;
        private System.Windows.Forms.GroupBox group_settings;
        private System.Windows.Forms.Label lb_parity;
        private System.Windows.Forms.Label lb_stopBits;
        private System.Windows.Forms.Label lb_dataBits;
        private System.Windows.Forms.Label lb_baudRate;
        private System.Windows.Forms.Label lb_portcom;
        private System.Windows.Forms.ComboBox cb_parity;
        private System.Windows.Forms.ComboBox cb_stopBits;
        private System.Windows.Forms.ComboBox cb_dataBits;
        private System.Windows.Forms.ComboBox cb_baudRate;
        private System.Windows.Forms.ComboBox cb_portCom;
        private System.Windows.Forms.Button btn_Open;
        private System.Windows.Forms.Button btn_close;
    }
}

